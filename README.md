# NOTICE THIS HAS MOVED TO https://gitlab.com/sas-blue/blue
# BLUE - Like Black, but for LabVIEW

LabVIEW autoformatter inspired by Python's [Black](https://github.com/psf/black) and [this](https://gitlab.com/felipe_public/lv-format-on-save) from Felipe. Also borrowed some code from the various [Nattify VIs](https://forums.ni.com/t5/Quick-Drop-Enthusiasts/Quick-Drop-Keyboard-Shortcut-Nattify-VI/m-p/3990402)

## Table of Contents

[TOC]

## Requirements

- G-CLI
- LabVIEW 2020 SP1 or newer
- Python 3.11 (maybe it work with older, not tested)
    - Watchdog (pip install watchdog)

## Usage

- clone this repo somewhere

- python3.11 -m pip install watchdog

- Run the blueMonitor.py passing it the folder to monitor for file changes or creation

``` bash
$python3.11 blueMonitor.py "folder"
```

or

- Run blue once on a folder

``` bash
g-cli "path_to_blue.vi" -- "folder"
```

Notes: Steps cannot handle renaming files, since it only operates on one file at a time, so it would mess with LabVIEW's linking (although note to self, if it worked on an entire project then perhaps... hmmm... although that does some problems, especially if done automatically - could easily break calling code that is not in a project.)
